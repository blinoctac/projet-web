var hierarchy =
[
    [ "App\\Middlewares\\AdminAccessMiddleware", "classApp_1_1Middlewares_1_1AdminAccessMiddleware.html", null ],
    [ "App\\Core\\App", "classApp_1_1Core_1_1App.html", null ],
    [ "App\\Core\\Auth", "classApp_1_1Core_1_1Auth.html", null ],
    [ "App\\Models\\Comment", "classApp_1_1Models_1_1Comment.html", null ],
    [ "App\\Repositories\\CommentRepository", "classApp_1_1Repositories_1_1CommentRepository.html", null ],
    [ "App\\Controllers\\API\\CommentsAPIController", "classApp_1_1Controllers_1_1API_1_1CommentsAPIController.html", null ],
    [ "App\\Composers\\CommentsComposer", "classApp_1_1Composers_1_1CommentsComposer.html", null ],
    [ "App\\Core\\Cookie", "classApp_1_1Core_1_1Cookie.html", null ],
    [ "App\\Middlewares\\CsrfProtection", "classApp_1_1Middlewares_1_1CsrfProtection.html", null ],
    [ "App\\Models\\Domain", "classApp_1_1Models_1_1Domain.html", null ],
    [ "App\\Controllers\\DomainController", "classApp_1_1Controllers_1_1DomainController.html", null ],
    [ "App\\Repositories\\DomainRepository", "classApp_1_1Repositories_1_1DomainRepository.html", null ],
    [ "App\\Composers\\FavoriteWinesComposer", "classApp_1_1Composers_1_1FavoriteWinesComposer.html", null ],
    [ "App\\Core\\File", "classApp_1_1Core_1_1File.html", null ],
    [ "App\\Core\\Headers", "classApp_1_1Core_1_1Headers.html", null ],
    [ "App\\Controllers\\HomeController", "classApp_1_1Controllers_1_1HomeController.html", null ],
    [ "App\\Core\\Pgsql", "classApp_1_1Core_1_1Pgsql.html", null ],
    [ "App\\Controllers\\API\\ProfileAPIController", "classApp_1_1Controllers_1_1API_1_1ProfileAPIController.html", null ],
    [ "App\\Controllers\\ProfileController", "classApp_1_1Controllers_1_1ProfileController.html", null ],
    [ "App\\Core\\Request", "classApp_1_1Core_1_1Request.html", null ],
    [ "App\\Core\\Response", "classApp_1_1Core_1_1Response.html", [
      [ "App\\Core\\Redirect", "classApp_1_1Core_1_1Redirect.html", null ]
    ] ],
    [ "App\\Core\\Route", "classApp_1_1Core_1_1Route.html", null ],
    [ "App\\Controllers\\API\\SearchAPIController", "classApp_1_1Controllers_1_1API_1_1SearchAPIController.html", null ],
    [ "App\\Controllers\\SearchController", "classApp_1_1Controllers_1_1SearchController.html", null ],
    [ "App\\Core\\Session", "classApp_1_1Core_1_1Session.html", null ],
    [ "App\\Models\\Type", "classApp_1_1Models_1_1Type.html", null ],
    [ "App\\Controllers\\TypeController", "classApp_1_1Controllers_1_1TypeController.html", null ],
    [ "App\\Repositories\\TypeRepository", "classApp_1_1Repositories_1_1TypeRepository.html", null ],
    [ "App\\Core\\Url", "classApp_1_1Core_1_1Url.html", null ],
    [ "App\\Models\\User", "classApp_1_1Models_1_1User.html", [
      [ "App\\Models\\LoggedUser", "classApp_1_1Models_1_1LoggedUser.html", null ]
    ] ],
    [ "App\\Middlewares\\UserAccessMiddleware", "classApp_1_1Middlewares_1_1UserAccessMiddleware.html", null ],
    [ "App\\Controllers\\API\\UserAPIController", "classApp_1_1Controllers_1_1API_1_1UserAPIController.html", null ],
    [ "App\\Repositories\\UserRepository", "classApp_1_1Repositories_1_1UserRepository.html", null ],
    [ "App\\Utils", "classApp_1_1Utils.html", null ],
    [ "App\\Models\\Wine", "classApp_1_1Models_1_1Wine.html", null ],
    [ "App\\Controllers\\API\\WineAPIController", "classApp_1_1Controllers_1_1API_1_1WineAPIController.html", null ],
    [ "App\\Controllers\\WineController", "classApp_1_1Controllers_1_1WineController.html", null ],
    [ "App\\Repositories\\WineRepository", "classApp_1_1Repositories_1_1WineRepository.html", null ],
    [ "App\\Models\\Year", "classApp_1_1Models_1_1Year.html", null ],
    [ "App\\Controllers\\YearController", "classApp_1_1Controllers_1_1YearController.html", null ],
    [ "App\\Repositories\\YearRepository", "classApp_1_1Repositories_1_1YearRepository.html", null ],
    [ "BladeOne", null, [
      [ "App\\Core\\Blade", "classApp_1_1Core_1_1Blade.html", null ]
    ] ],
    [ "Iterator", null, [
      [ "App\\Core\\PdoIterator", "classApp_1_1Core_1_1PdoIterator.html", null ]
    ] ]
];