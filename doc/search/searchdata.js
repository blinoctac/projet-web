var indexSectionsWithContent =
{
  0: "$_abcdefghijklmnopqrstuvwy",
  1: "abcdfhlprstuwy",
  2: "a",
  3: "abcdfhilmnprstuwy",
  4: "_abcdefghijklmnopqrstuvw",
  5: "$_ahilmprsuv",
  6: "lt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Espaces de nommage",
  3: "Fichiers",
  4: "Fonctions",
  5: "Variables",
  6: "Pages"
};

