var dir_fc6199fba97859a095e1d9a5aa5fae23 =
[
    [ "Comment.php", "Comment_8php.html", [
      [ "Comment", "classApp_1_1Models_1_1Comment.html", "classApp_1_1Models_1_1Comment" ]
    ] ],
    [ "Domain.php", "Domain_8php.html", [
      [ "Domain", "classApp_1_1Models_1_1Domain.html", "classApp_1_1Models_1_1Domain" ]
    ] ],
    [ "LoggedUser.php", "LoggedUser_8php.html", [
      [ "LoggedUser", "classApp_1_1Models_1_1LoggedUser.html", "classApp_1_1Models_1_1LoggedUser" ]
    ] ],
    [ "Type.php", "Type_8php.html", [
      [ "Type", "classApp_1_1Models_1_1Type.html", "classApp_1_1Models_1_1Type" ]
    ] ],
    [ "User.php", "User_8php.html", [
      [ "User", "classApp_1_1Models_1_1User.html", "classApp_1_1Models_1_1User" ]
    ] ],
    [ "Wine.php", "Wine_8php.html", [
      [ "Wine", "classApp_1_1Models_1_1Wine.html", "classApp_1_1Models_1_1Wine" ]
    ] ],
    [ "Year.php", "Year_8php.html", [
      [ "Year", "classApp_1_1Models_1_1Year.html", "classApp_1_1Models_1_1Year" ]
    ] ]
];