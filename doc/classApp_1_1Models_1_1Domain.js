var classApp_1_1Models_1_1Domain =
[
    [ "fromDbRow", "classApp_1_1Models_1_1Domain.html#a167889475aa610f394840128eea8cb03", null ],
    [ "getId", "classApp_1_1Models_1_1Domain.html#ac89284c0eb17516819c16fdb9cdeb551", null ],
    [ "getImagePath", "classApp_1_1Models_1_1Domain.html#a0f4bb68e90537abdd9864d79a7cbab19", null ],
    [ "getImagePathFromId", "classApp_1_1Models_1_1Domain.html#a9f1527987ea906dfb31029b2b40e39a0", null ],
    [ "getName", "classApp_1_1Models_1_1Domain.html#ab03f708b332ef882f8e8fd673c70d5ef", null ],
    [ "getTags", "classApp_1_1Models_1_1Domain.html#a2bf8e7d9334955a62e5d1f1297551820", null ],
    [ "setId", "classApp_1_1Models_1_1Domain.html#ad18629648d20484562c0fc6406d9a43d", null ],
    [ "setName", "classApp_1_1Models_1_1Domain.html#a93ecbec67cdab9ae805392f0de8d1668", null ],
    [ "setTags", "classApp_1_1Models_1_1Domain.html#ab157aa920f208851d9b1dbf25e95feb9", null ],
    [ "$id", "classApp_1_1Models_1_1Domain.html#a4b44e639eaf78bca557f3575a8e07228", null ],
    [ "$name", "classApp_1_1Models_1_1Domain.html#a3e30eb16b272adb7af4bc489636cb9ca", null ],
    [ "$tags", "classApp_1_1Models_1_1Domain.html#a474cff87a2d3819f9452781ffc939e93", null ]
];