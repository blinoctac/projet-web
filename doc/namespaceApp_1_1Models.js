var namespaceApp_1_1Models =
[
    [ "Comment", "classApp_1_1Models_1_1Comment.html", "classApp_1_1Models_1_1Comment" ],
    [ "Domain", "classApp_1_1Models_1_1Domain.html", "classApp_1_1Models_1_1Domain" ],
    [ "LoggedUser", "classApp_1_1Models_1_1LoggedUser.html", "classApp_1_1Models_1_1LoggedUser" ],
    [ "Type", "classApp_1_1Models_1_1Type.html", "classApp_1_1Models_1_1Type" ],
    [ "User", "classApp_1_1Models_1_1User.html", "classApp_1_1Models_1_1User" ],
    [ "Wine", "classApp_1_1Models_1_1Wine.html", "classApp_1_1Models_1_1Wine" ],
    [ "Year", "classApp_1_1Models_1_1Year.html", "classApp_1_1Models_1_1Year" ]
];