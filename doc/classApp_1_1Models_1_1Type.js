var classApp_1_1Models_1_1Type =
[
    [ "fromDbRow", "classApp_1_1Models_1_1Type.html#af57eda5382b369e09100f16be3e058bd", null ],
    [ "getId", "classApp_1_1Models_1_1Type.html#a39a3e822e52d11cfa216e67895ac1bb4", null ],
    [ "getImagePath", "classApp_1_1Models_1_1Type.html#ad9456bf61ec11595349776314a7a61fb", null ],
    [ "getImagePathFromId", "classApp_1_1Models_1_1Type.html#ae53416a23e0b4d64fa48bbda92b783b8", null ],
    [ "getName", "classApp_1_1Models_1_1Type.html#a2392cfb683ec20b620f27b98f5521402", null ],
    [ "getTags", "classApp_1_1Models_1_1Type.html#a08edbce6ef70ffc25468f363d422ea63", null ],
    [ "setId", "classApp_1_1Models_1_1Type.html#a684f3f58f0c8c2fab71f3e2691844a41", null ],
    [ "setName", "classApp_1_1Models_1_1Type.html#a2610c3e055366cc3fc49d028ae986b2d", null ],
    [ "setTags", "classApp_1_1Models_1_1Type.html#aa572143ffcf5ff525e4407aaca75121c", null ],
    [ "$id", "classApp_1_1Models_1_1Type.html#ad6e41b4675dd53042ddb1ef296dbcb67", null ],
    [ "$name", "classApp_1_1Models_1_1Type.html#a5ee4e1565677b19bc144aa12e01a5f83", null ],
    [ "$tags", "classApp_1_1Models_1_1Type.html#a9978745bc5f741b7d8cb15ff006fae9f", null ]
];