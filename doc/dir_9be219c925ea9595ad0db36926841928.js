var dir_9be219c925ea9595ad0db36926841928 =
[
    [ "CommentsAPIController.php", "CommentsAPIController_8php.html", [
      [ "CommentsAPIController", "classApp_1_1Controllers_1_1API_1_1CommentsAPIController.html", "classApp_1_1Controllers_1_1API_1_1CommentsAPIController" ]
    ] ],
    [ "ProfileAPIController.php", "ProfileAPIController_8php.html", [
      [ "ProfileAPIController", "classApp_1_1Controllers_1_1API_1_1ProfileAPIController.html", "classApp_1_1Controllers_1_1API_1_1ProfileAPIController" ]
    ] ],
    [ "SearchAPIController.php", "SearchAPIController_8php.html", [
      [ "SearchAPIController", "classApp_1_1Controllers_1_1API_1_1SearchAPIController.html", "classApp_1_1Controllers_1_1API_1_1SearchAPIController" ]
    ] ],
    [ "UserAPIController.php", "UserAPIController_8php.html", [
      [ "UserAPIController", "classApp_1_1Controllers_1_1API_1_1UserAPIController.html", "classApp_1_1Controllers_1_1API_1_1UserAPIController" ]
    ] ],
    [ "WineAPIController.php", "WineAPIController_8php.html", [
      [ "WineAPIController", "classApp_1_1Controllers_1_1API_1_1WineAPIController.html", "classApp_1_1Controllers_1_1API_1_1WineAPIController" ]
    ] ]
];