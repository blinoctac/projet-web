var namespaceApp_1_1Middlewares =
[
    [ "AdminAccessMiddleware", "classApp_1_1Middlewares_1_1AdminAccessMiddleware.html", "classApp_1_1Middlewares_1_1AdminAccessMiddleware" ],
    [ "CsrfProtection", "classApp_1_1Middlewares_1_1CsrfProtection.html", "classApp_1_1Middlewares_1_1CsrfProtection" ],
    [ "UserAccessMiddleware", "classApp_1_1Middlewares_1_1UserAccessMiddleware.html", "classApp_1_1Middlewares_1_1UserAccessMiddleware" ]
];