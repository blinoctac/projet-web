var classApp_1_1Models_1_1Year =
[
    [ "fromDbRow", "classApp_1_1Models_1_1Year.html#a589cc0af18d5a2c1778df477820ad7ab", null ],
    [ "getId", "classApp_1_1Models_1_1Year.html#ab8fe1cfac6c3666f365392e3b112f71f", null ],
    [ "getImagePath", "classApp_1_1Models_1_1Year.html#ac8de3e123ac245a573a38ccbc82e44c4", null ],
    [ "getImagePathFromId", "classApp_1_1Models_1_1Year.html#a3a186398bd3c9fcdc380456a3a8bcefb", null ],
    [ "getTags", "classApp_1_1Models_1_1Year.html#afa610efdf584ce5f5fefaf0a6190b231", null ],
    [ "getYear", "classApp_1_1Models_1_1Year.html#a9576ee97cf85f4d51ca29a6cf3893b39", null ],
    [ "setId", "classApp_1_1Models_1_1Year.html#aeaca364fcf5a8d289a13ad453b9985f8", null ],
    [ "setTags", "classApp_1_1Models_1_1Year.html#affa3178d1dc43aaafed9145adf11193f", null ],
    [ "setYear", "classApp_1_1Models_1_1Year.html#aab34aaff90446713a652e9b3c18b95a6", null ],
    [ "$id", "classApp_1_1Models_1_1Year.html#ac0ba00dcd701a90ede695fe538b8af86", null ],
    [ "$tags", "classApp_1_1Models_1_1Year.html#a7603c9c950290599f4d862e60f1ec693", null ],
    [ "$year", "classApp_1_1Models_1_1Year.html#aad1010aa18834652e93da3ea643ad752", null ]
];