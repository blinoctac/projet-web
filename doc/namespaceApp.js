var namespaceApp =
[
    [ "Composers", "namespaceApp_1_1Composers.html", "namespaceApp_1_1Composers" ],
    [ "Controllers", "namespaceApp_1_1Controllers.html", "namespaceApp_1_1Controllers" ],
    [ "Core", "namespaceApp_1_1Core.html", "namespaceApp_1_1Core" ],
    [ "Middlewares", "namespaceApp_1_1Middlewares.html", "namespaceApp_1_1Middlewares" ],
    [ "Models", "namespaceApp_1_1Models.html", "namespaceApp_1_1Models" ],
    [ "Repositories", "namespaceApp_1_1Repositories.html", "namespaceApp_1_1Repositories" ],
    [ "Utils", "classApp_1_1Utils.html", "classApp_1_1Utils" ]
];