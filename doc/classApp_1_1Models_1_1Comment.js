var classApp_1_1Models_1_1Comment =
[
    [ "fromDbRow", "classApp_1_1Models_1_1Comment.html#a99d25b05e228c75706f79b26b90f1908", null ],
    [ "getDomain", "classApp_1_1Models_1_1Comment.html#a9988f97547623e8b4b5926cb2b887e7a", null ],
    [ "getId", "classApp_1_1Models_1_1Comment.html#aa7ec916ff242ad597045f5092a5fc0b1", null ],
    [ "getMsg", "classApp_1_1Models_1_1Comment.html#a1790ca76132197dda6cb8a6457ea29a2", null ],
    [ "getPostedAt", "classApp_1_1Models_1_1Comment.html#a06a44e1e74ac263956a7639a6680dd35", null ],
    [ "getReplyTo", "classApp_1_1Models_1_1Comment.html#a200213bdcfc6c24002afeaa996f05074", null ],
    [ "getType", "classApp_1_1Models_1_1Comment.html#a6faf1b350afa1aa260dee6fe22418df1", null ],
    [ "getUser", "classApp_1_1Models_1_1Comment.html#acef3258391fd706b02d366722ef28267", null ],
    [ "getWine", "classApp_1_1Models_1_1Comment.html#a7b69109964b5f04e18dfa3f03f82b7c3", null ],
    [ "getYear", "classApp_1_1Models_1_1Comment.html#a2bcb8e772cd62b4726d8d69d96d10641", null ],
    [ "setDomain", "classApp_1_1Models_1_1Comment.html#abc1cb92d508e4d7341e1b1adbbb5a0df", null ],
    [ "setId", "classApp_1_1Models_1_1Comment.html#a2812e0bcb7e6d4d79dfdc0b35a7ee967", null ],
    [ "setMsg", "classApp_1_1Models_1_1Comment.html#a37509621e5fe3731047d1bef325f8f9a", null ],
    [ "setPostedAt", "classApp_1_1Models_1_1Comment.html#a7392984993697e88bf668dd84de59a0b", null ],
    [ "setReplyTo", "classApp_1_1Models_1_1Comment.html#a6cf62fa7e2f0a9a9c06fa97532144be1", null ],
    [ "setType", "classApp_1_1Models_1_1Comment.html#ad9af858a1ecc37efbe0286bc16f4eb52", null ],
    [ "setUser", "classApp_1_1Models_1_1Comment.html#a047773fe00a82da96af8b996e30e0b3e", null ],
    [ "setWine", "classApp_1_1Models_1_1Comment.html#a0d13274f9e3b545a4cf3a03c98dc9fc3", null ],
    [ "setYear", "classApp_1_1Models_1_1Comment.html#a55e988fe341ccc868922dfd4a640fa7f", null ],
    [ "toDbRow", "classApp_1_1Models_1_1Comment.html#a80fa93eb583759461671099448c4cb38", null ],
    [ "$domainId", "classApp_1_1Models_1_1Comment.html#a9adc9035d7dd92da4624d1af30e5c669", null ],
    [ "$id", "classApp_1_1Models_1_1Comment.html#a87c44639b7d434294c707794813f6f50", null ],
    [ "$in_response_toId", "classApp_1_1Models_1_1Comment.html#aaaaba229a4ecb2a414ca155cded8e61f", null ],
    [ "$msg", "classApp_1_1Models_1_1Comment.html#afef4183375182c4e5201916742581474", null ],
    [ "$posted_at", "classApp_1_1Models_1_1Comment.html#ae0c2922c0f61bc2741c96589a3f8d11e", null ],
    [ "$typeId", "classApp_1_1Models_1_1Comment.html#ad64f93beb82284a36f3ae27ed821662a", null ],
    [ "$userId", "classApp_1_1Models_1_1Comment.html#a50901d3bc3669b8fa6e5bdcf5062bdae", null ],
    [ "$wineId", "classApp_1_1Models_1_1Comment.html#a5d2e297a14f9a5a4d60eaf04aaed7f58", null ],
    [ "$yearId", "classApp_1_1Models_1_1Comment.html#a1b35bf25b3d9beff2fa3cae5c0253306", null ]
];