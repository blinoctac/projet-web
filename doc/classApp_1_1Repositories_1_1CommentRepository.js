var classApp_1_1Repositories_1_1CommentRepository =
[
    [ "__construct", "classApp_1_1Repositories_1_1CommentRepository.html#ae0a62d9f76a9152f78ccdb76df3061fd", null ],
    [ "addComment", "classApp_1_1Repositories_1_1CommentRepository.html#aef2088e7f7d180f08d559c5c1f19835a", null ],
    [ "deleteComment", "classApp_1_1Repositories_1_1CommentRepository.html#aa7d79cf68bb1ce58c52901e10002724c", null ],
    [ "get", "classApp_1_1Repositories_1_1CommentRepository.html#ae2639b3fa93dac9232b03c90f68e8b88", null ],
    [ "getAllComments", "classApp_1_1Repositories_1_1CommentRepository.html#a939fcf85e18c90d0d94df2e1c141d5e6", null ],
    [ "getCommentsFor", "classApp_1_1Repositories_1_1CommentRepository.html#a42730e3555433550bdda27254cca5351", null ],
    [ "getCountPostedByUser", "classApp_1_1Repositories_1_1CommentRepository.html#a2100fc4f889e48391d3b98f3ee8499c0", null ],
    [ "getLikes", "classApp_1_1Repositories_1_1CommentRepository.html#a1b308831149f5a999b5cf8a1f71671bd", null ],
    [ "getMostLiked", "classApp_1_1Repositories_1_1CommentRepository.html#acd0b46c839f6a60072371e87b61448c8", null ],
    [ "getResults", "classApp_1_1Repositories_1_1CommentRepository.html#aa8a6153c01a6a564977ec7997f336583", null ],
    [ "isLiked", "classApp_1_1Repositories_1_1CommentRepository.html#a0689cbecd99f20979966eecda5325a98", null ],
    [ "saveComment", "classApp_1_1Repositories_1_1CommentRepository.html#a4607c04bc18a5b355b8d73b7e303d824", null ],
    [ "setLiked", "classApp_1_1Repositories_1_1CommentRepository.html#a842e51ed72e9d04bb3da768564929556", null ],
    [ "$dbAdapter", "classApp_1_1Repositories_1_1CommentRepository.html#a2adbc0076ee22a2e2d57a5a4130f169e", null ]
];