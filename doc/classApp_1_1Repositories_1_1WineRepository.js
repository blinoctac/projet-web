var classApp_1_1Repositories_1_1WineRepository =
[
    [ "__construct", "classApp_1_1Repositories_1_1WineRepository.html#a9618c8811958423e84437a3d6153a1e8", null ],
    [ "addWine", "classApp_1_1Repositories_1_1WineRepository.html#ab3af5c84e17979504345a1db41a088e1", null ],
    [ "delete", "classApp_1_1Repositories_1_1WineRepository.html#a019506c3d0fd4da1cbb780ad41031142", null ],
    [ "get", "classApp_1_1Repositories_1_1WineRepository.html#ae1666af61c53752e24f8516221b02f1d", null ],
    [ "getAllFavorites", "classApp_1_1Repositories_1_1WineRepository.html#a87e2d021ce749e5872e1f248f2142131", null ],
    [ "getFavoriteCountByUser", "classApp_1_1Repositories_1_1WineRepository.html#aa17f50f2a08a85727522e47dea4ad107", null ],
    [ "getFavoriteCountByWine", "classApp_1_1Repositories_1_1WineRepository.html#acd5f92146e8ae9f44030b5c6f81c877e", null ],
    [ "getFavoriteWinesByUser", "classApp_1_1Repositories_1_1WineRepository.html#aad5cba7645dab41cae9725c7374e4ed9", null ],
    [ "getLast", "classApp_1_1Repositories_1_1WineRepository.html#abc44daa7e24d6f91316ea7853a2a8bc4", null ],
    [ "getProposedCountByUser", "classApp_1_1Repositories_1_1WineRepository.html#a0025e76d1dd950cdf0ebab3697fb5d9b", null ],
    [ "isFavorite", "classApp_1_1Repositories_1_1WineRepository.html#a4da235da780c4c3481829e9435ad23bb", null ],
    [ "saveWine", "classApp_1_1Repositories_1_1WineRepository.html#acbe027ede6f8050fc7a198a2ce5dc605", null ],
    [ "search", "classApp_1_1Repositories_1_1WineRepository.html#a7624cae593dd170444404169bc6020a4", null ],
    [ "setFavorite", "classApp_1_1Repositories_1_1WineRepository.html#ac2dd82ae800bb03213ccdfb272a96538", null ],
    [ "$dbAdapter", "classApp_1_1Repositories_1_1WineRepository.html#a3dc995fb88f043729eb9033fe0ea5d71", null ]
];