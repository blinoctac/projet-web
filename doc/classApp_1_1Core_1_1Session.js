var classApp_1_1Core_1_1Session =
[
    [ "__construct", "classApp_1_1Core_1_1Session.html#a5624bb0cf38a86a40890a68f1e024f9d", null ],
    [ "clear", "classApp_1_1Core_1_1Session.html#ac70406a46cf2a1af5b6f7508d6df1de7", null ],
    [ "destroy", "classApp_1_1Core_1_1Session.html#ac377d2e69078ce1746bb3ccaa98f811e", null ],
    [ "flash", "classApp_1_1Core_1_1Session.html#a369c9a5eb0211ffa433aa37fa597090c", null ],
    [ "forget", "classApp_1_1Core_1_1Session.html#afbd061711eccec75186f18975fd36695", null ],
    [ "get", "classApp_1_1Core_1_1Session.html#af52026350712ed9eaf57e02ddec66510", null ],
    [ "getSessionId", "classApp_1_1Core_1_1Session.html#a8f6193dcd8ffdf5d1c5c74aff66385eb", null ],
    [ "has", "classApp_1_1Core_1_1Session.html#a8fec33149299ad5e3d6cdeb66d9a8045", null ],
    [ "isStarted", "classApp_1_1Core_1_1Session.html#a051b36b37efb3207df0169c58d3c5af6", null ],
    [ "me", "classApp_1_1Core_1_1Session.html#a086e7a862ecce8bc2551f7a5025d89d2", null ],
    [ "mySet", "classApp_1_1Core_1_1Session.html#ad74289b28b7f4d37b2236babfa97390b", null ],
    [ "regenerate", "classApp_1_1Core_1_1Session.html#ab2f483f6ec8112243aa11f23758952b3", null ],
    [ "set", "classApp_1_1Core_1_1Session.html#af386d08419abfdbb97ff63383c0b7510", null ],
    [ "start", "classApp_1_1Core_1_1Session.html#ac10617407da059e188b13a9ba6a91425", null ],
    [ "unset", "classApp_1_1Core_1_1Session.html#a03172b3e7d53e2beb67ec4c366412938", null ],
    [ "$isStarted", "classApp_1_1Core_1_1Session.html#abd110574a946ddc1b603ca4181f8a084", null ]
];