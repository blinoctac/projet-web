var namespaceApp_1_1Repositories =
[
    [ "CommentRepository", "classApp_1_1Repositories_1_1CommentRepository.html", "classApp_1_1Repositories_1_1CommentRepository" ],
    [ "DomainRepository", "classApp_1_1Repositories_1_1DomainRepository.html", "classApp_1_1Repositories_1_1DomainRepository" ],
    [ "TypeRepository", "classApp_1_1Repositories_1_1TypeRepository.html", "classApp_1_1Repositories_1_1TypeRepository" ],
    [ "UserRepository", "classApp_1_1Repositories_1_1UserRepository.html", "classApp_1_1Repositories_1_1UserRepository" ],
    [ "WineRepository", "classApp_1_1Repositories_1_1WineRepository.html", "classApp_1_1Repositories_1_1WineRepository" ],
    [ "YearRepository", "classApp_1_1Repositories_1_1YearRepository.html", "classApp_1_1Repositories_1_1YearRepository" ]
];