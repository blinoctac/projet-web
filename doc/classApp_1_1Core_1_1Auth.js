var classApp_1_1Core_1_1Auth =
[
    [ "_loginSession", "classApp_1_1Core_1_1Auth.html#a07151a8e40157e4aa9bce7811b21d515", null ],
    [ "_logoutSession", "classApp_1_1Core_1_1Auth.html#a31ef135ade6d6f92976ff8feb6f72d96", null ],
    [ "canEdit", "classApp_1_1Core_1_1Auth.html#a40bb958135730d237e8faaeb4ec9ce6d", null ],
    [ "isAdmin", "classApp_1_1Core_1_1Auth.html#a068bb918ae3720c45c6a2a5a21c40b40", null ],
    [ "isLogged", "classApp_1_1Core_1_1Auth.html#a01daf403a75943278f3f05fd8ab5e024", null ],
    [ "loggedUser", "classApp_1_1Core_1_1Auth.html#aef5ff1f7ba353dd8cdb909080ebf7dc4", null ],
    [ "LAST_LOGGED_KEY", "classApp_1_1Core_1_1Auth.html#a7f655974c34e82cbab9c527748924e93", null ],
    [ "USER_KEY", "classApp_1_1Core_1_1Auth.html#a43b81ee3debc2b5a626161f7c6611699", null ]
];