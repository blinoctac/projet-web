<footer class="footer " data-background-color="black">
    <div class="container">
        <nav>
            <ul>
                <li>
                    <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
                        Notre projet
                    </a>
                </li>
                <li>
                    <a href="mailto:eleves@iiens.net">
                        Nous contacter
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright">
            OenologIIE &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
        </div>
    </div>
</footer>