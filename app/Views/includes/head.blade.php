<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="@asset('img/apple-icon.png')">
<link rel="icon" type="image/png" href="@asset('img/favicon.png')">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>@yield('title') - Oenologiie</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
<!--     Fonts and icons     -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<!-- CSS Files -->
<link href="@asset('css/bootstrap.min.css')" rel="stylesheet" />
<link href="@asset('css/bootstrap-addons.css')" rel="stylesheet" />
<link href="@asset('css/style.css')" rel="stylesheet" />
<link href="@asset('css/font-awesome.min.css')" rel="stylesheet" />